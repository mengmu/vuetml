/*
全站路由处理
order by qinhaozhi Data:20170915
(Remember to write down each change)
*/
import Vue from 'vue'
import Router from 'vue-router'


const main = () => import('@/page/main/main.vue') // appTest
const notFound = () => import('@/page/notFound/notFound') // 404页面

Vue.use(Router)

console.log(process.env.URL_BASE);
export default new Router({
  mode: 'history',
  base: '/' + process.env.URL_BASE,
  routes: [
    // {
    //   path: '/CZD',
    //   meta: {
    //     title: ""
    //   },
    //   component: CZDindex,
    // },
    // path: '/',
    // component: 
    {
      path: '/',
      meta: {
        title: ""
      },
      component: main,
    },
    {
      path: '*',
      meta: {
        title: "",
      },
      component: notFound
    },
  ]

})
