let mscommon = {
  /**
   * 存储localStorage
   */
  setStore: (name, content) => {
    if (!name) return;
    if (typeof content !== 'string') {
      content = JSON.stringify(content);
    }
    window.localStorage.setItem(name, content);
  },

  /**
   * 获取localStorage
   */
  getStore: name => {
    if (!name) return;
    let value = window.localStorage.getItem(name);
    try {
      value = JSON.parse(value);
      return value;
    } catch (e) {
      console.log(e);
      return value;
    }
  },

  /**
   * 删除localStorage
   */
  removeStore: name => {
    if (!name) return;
    window.localStorage.removeItem(name);
  },

  //设置sessionStorage
  setSession: (name, content) => {
    if (!name) return;
    if (typeof content !== 'string') {
      content = JSON.stringify(content);
    }
    window.sessionStorage.setItem(name, content);
  },
  /**
   * 获取sessionStorage
   */
  getSession: name => {
    if (!name) return;
    let value = window.sessionStorage.getItem(name);
    try {
      value = JSON.parse(value);
      return value;
    } catch (e) {
      console.log(e);
      return value;
    }
  },

  /**
   * 删除sessionStorage
   */
  removeSession: name => {
    if (!name) return;
    window.sessionStorage.removeItem(name);
  },
  // 获取URL后面参数
  GetQueryString: name => {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
  },
  //获取host
  getHost: function() {
    var _http = window.location.protocol;
    var _hostname = window.location.hostname;
    var _port = window.location.port;
    var _pathname = window.location.pathname;
    var http = "";
    if (_port) {
      return http = _http + "//" + _hostname + ":" + _port + "/";
    }
    return http = _http + "//" + _hostname + "/";
  },
  //rem转换px
  rem2px: function(rem) {
    var clientWidth = document.documentElement.clientWidth;
    if (clientWidth) {
      return clientWidth / 750 * 100 * rem;
    } else {
      return rem;
    }
  },
  /**
   * 获取style样式
   */
  getStyle: (element, attr, NumberMode = 'int') => {
    let target;
    // scrollTop 获取方式不同，没有它不属于style，而且只有document.body才能用
    if (attr === 'scrollTop') {
      target = element.scrollTop;
    } else if (element.currentStyle) {
      target = element.currentStyle[attr];
    } else {
      target = document.defaultView.getComputedStyle(element, null)[attr];
    }
    //在获取 opactiy 时需要获取小数 parseFloat
    return NumberMode == 'float' ? parseFloat(target) : parseInt(target);
  },
  // 获取滚动dom infiniteScroll里使用
  getScrollview: function(el) {
    let currentNode = el;
    while (currentNode && currentNode.tagName !== 'HTML' && currentNode.tagName !== 'BODY' && currentNode.nodeType === 1) {
      let overflowY = document.defaultView.getComputedStyle(currentNode).overflowY;
      if (overflowY === 'scroll' || overflowY === 'auto') {
        return currentNode;
      }
      currentNode = currentNode.parentNode;
    }
    return window;
  },

  /* istanbul ignore next */
  once: function(el, event, fn) {
    var listener = function() {
      if (fn) {
        fn.apply(this, arguments);
      }
      off(el, event, listener);
    };
    on(el, event, listener);
  },



  hasClass: function(elem, cls) {
    cls = cls || '';
    if (cls.replace(/\s/g, '').length == 0) return false;
    return new RegExp(' ' + cls + ' ').test(' ' + elem.className + ' ');
  },

  addClass: function(ele, cls) {
    if (!hasClass(ele, cls)) {
      ele.className = ele.className == '' ? cls : ele.className + ' ' + cls;
    }
  },

  removeClass: function(ele, cls) {
    if (hasClass(ele, cls)) {
      let newClass = ' ' + ele.className.replace(/[\t\r\n]/g, '') + ' ';
      while (newClass.indexOf(' ' + cls + ' ') >= 0) {
        newClass = newClass.replace(' ' + cls + ' ', ' ');
      }
      ele.className = newClass.replace(/^\s+|\s+$/g, '');
    }
  },

}

export default mscommon
